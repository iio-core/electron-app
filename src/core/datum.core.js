import sublevel from 'subleveldown'
import queryBuilder from 'mongo-query-compiler'
import moment from 'moment'
import _ from 'lodash'
import ObjectID from 'bson-objectid'
import fs from 'fs'
import path from 'path'

export default class Datum {
  constructor(syncdb, collection) {
    this.name = collection

    this.$syncdb = syncdb

    if (this.$syncdb._db) {
      this._db = sublevel(this.$syncdb._db, collection, { valueEncoding: 'json' })
    }

    this._deleted = []

    this.$syncdb.$services.$on('app:disconnected', () => {
      console.log('sync disconnected event')
    })

    this.$syncdb.$services.$on('app:connected', async () => {
      console.log('sync connected event for [' + this.name + ']')
      // mustSync: update by the first syncFromRemote from syncdb plugin
      if (this.mustSync) {
        try {
          await this._syncToRemote()
          await this._syncFromRemote()
        } catch(err) {
          console.log(err)
        }
      }
    })

    console.log('create local collection [' + collection + ']')
  }

  _find(query) {
    return new Promise((resolve, reject) => {
      let docs = []
      let filter
      if (query && JSON.stringify(query) !== '{}') {
        filter = queryBuilder(query)
      }

      let readStream = this._db.createValueStream()

      readStream.on('data',function(e) {
        if (filter && filter(e)) {
          docs.push(e)
        } else if (!filter) {
          docs.push(e)
        }
      }).on('error', function(err) {
        reject(err)
      }).on('close', () => {
        resolve(docs)
      })
    })
  }

  _findOne(query) {
    return new Promise((resolve, reject) => {
      let filter
      let found
      if (query && JSON.stringify(query) !== '{}') {
        filter = queryBuilder(query)
      }

      let readStream = this._db.createValueStream()
      readStream.on('data', function(e) {
        if (filter && filter(e)) {
          found = e
          resolve(e)
          readStream.destroy()
        } else if (!filter) {
          found = e
          resolve(e)
          readStream.destroy()
        }
      }).on('error', function(err) {
        reject(err)
      }).on('close', () => {
        if (!found) {
          reject(new Error('not found'))
        }
      })
    })
  }

  _syncToRemote() {
    return new Promise((resolve, reject) => {
      console.log('collection sync TO remote', this.name)
      console.time(this.name + '-put')
      this._find({ _notSynched: true }).then(async updated => {
        try {
          let dataModule = await this.$syncdb.$modules.waitForService('data')
          await dataModule[this.name].sync({
            updated: updated,
            deleted: this._deleted
          })

          resolve()
        } catch (err) {
          reject(err)
        }
      }).catch(err => reject())
    })
    console.timeEnd(this.name + '-put')
  }

  async _syncFromRemote(remote) {
    console.log('collection sync FROM remote', this.name)
    console.time(this.name + '-get')
    try {
      let dataModule = await this.$syncdb.$modules.waitForService('data')
      remote = remote || await dataModule[this.name]
      let docs = await remote.find({})
      for (let doc of docs) {
        this._db.get(doc._id, (err, existing) => {
          if (err) {
            this._db.put(doc._id, doc, err => {
              if (err) {
                console.log('error synching element', doc._id, err)
              }
            })
          } else {
            let delta = moment(existing._lastModified).diff(moment(doc._lastModified))
            if (delta < 0) {
              this._db.put(doc._id, doc, err => {
                if (err) console.log('failed to synch', doc._id)
              })
              // console.log('updated', this.name, doc._id, existing)
            } /* else {
              console.log(doc._id + ' is the most recent version',
                existing._lastModified, doc._lastModified, delta)
            } */
          }
        })
      }
      console.log(this.name + ' length= ', docs.length)
    } catch (err) {
      console.error(err, '[' + this.name + ']')
    }
    console.timeEnd(this.name + '-get')
  }

  put(args) {
    return new Promise(async (resolve, reject) => {
      args._id = args._id || new ObjectID()
      args._lastModified = new Date()

      // silently update remote if possible
      this.$syncdb.$modules.waitForService('data').then(async dataModule => {
        let remote = await dataModule[this.name]
        let id = (await remote.put(args))._id
        let dataBack = await remote.get({ _id: id })
        args._lastModified = dataBack._lastModified
        console.log('updated ' + id + ' remotely (lastModified=' + args._lastModified + ')')
      }).catch(err => {
        console.log('try to update remote on put, but failed', err)
        args._notSynched = true
      })

      this._db.put(args._id, args, async err => {
        if (err) {
          reject(err)
        } else {
          resolve({ _id: args._id })
        }
      })
    })
  }

  find(args) {
    return new Promise(async (resolve, reject) => {
      this._find(args).then(docs => {
        resolve(docs)
      }).catch(err => reject(err))
    })
  }

  get(args) {
    return new Promise(async (resolve, reject) => {
      if (args._id) {
        this._db.get(args._id, (err, doc) => {
          if (err) {
            reject(err)
          } else {
            resolve(doc)
          }
        })
      } else {
        this._findOne(args).then(doc => {
          resolve(doc)
        }).catch(err => reject(err))
      }
    })
  }

  _get(id) {
    return new Promise(async (resolve, reject) => {
      this._db.get(id, (err, doc) => {
        if (err) {
          reject(err)
        } else {
          resolve(doc)
        }
      })
    })
  }

  del(args) {
    return new Promise(async (resolve, reject) => {
      let id = args._id
      let done
      if (!args._id) {
        let doc = await this._findOne(args)
        id = doc._id
      }

      // silently update remote if possible
      this.$syncdb.$modules.waitForService('data').then(async dataModule => {
        let remote = await dataModule[this.name]
        await remote.del({ _id: id })
        done = true
        console.log('deleted [' + id + '] remotely')
      }).catch(err => {
        console.log('try to update remote on delete, but failed', err)
      })

      this._db.del(id, err => {
        if (err) {
          reject(err)
        } else {
          resolve({ _id: id })
          if (!done && this._deleted.indexOf(id) < 0) {
            this._deleted.push(id)
          }
        }
      })
    })
  }

  async _dumpToFile(outputPath) {
    try {
      let elements = await this._find({})

      fs.writeFileSync(path.join(process.cwd(), outputPath || 'data_' + this.name + '.json'),
        JSON.stringify(elements, null, 2), 'utf8')
    } catch (err) {
      console.log(err)
    }
  }
}
