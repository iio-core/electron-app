import {EventEmitter} from 'events'
import {uuid} from '../utils'

/* Module base class */
export default class Module extends EventEmitter {
  constructor(io, options) {
    super()

    try {
      this._options = options || {}

      // module name
      this._name = this._options.name || Math.random().toString(36).slice(2)

      // public options send to client
      this._publicOptions = this._options.publicOptions

      // sets an unique id for the module
      this.uuid = uuid()

    } catch (err) {
      console.error(err, 'Falied to build module. Exiting...', this._name)
      process.exit(1)
    }

    // console.log('[' + this._name + '] has been defined with uuid= ' + this.uuid)
  }

  get name() {
    return this._name
  }
}
