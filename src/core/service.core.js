import _ from 'lodash'
import {EventEmitter} from 'events'
import {uuid} from '../utils'
import {getAllMethods} from '../utils'

/* Unified service base class */
export default class Service extends EventEmitter {
  constructor(io, options) {
    super()

    try {
      this._options = options || {}

      // reference to the client WS
      this._io = io

      // service name
      this._name = this._options.name || Math.random().toString(36).slice(2)

      // service public methods
      this._methods = []

      // public options send to client
      this._publicOptions = this._options.publicOptions

      // sets an unique id for the service
      this.uuid = uuid()

      // detect methods to be exposed as per design rules
      this._detectMethods()
    } catch (err) {
      console.error(err, 'Falied to build service. Exiting...', this._name)
      process.exit(1)
    }

    // console.log('[' + this._name + '] has been defined with uuid= ' + this.uuid)
  }

  get name() {
    return this._name
  }

  async _checkAccess(method, userId) {
    return new Promise((resolve, reject) => {
      this.$app.$syncdb.users.get({ _id: userId }).then(user => {
        if (user) {
          this.$app.$syncdb.roles.get({}).then(roles => {
            if (roles[user.role] && roles[user.role][this._name]) {
              if (_.find(roles[user.role][this._name].exec, e => { return e === method })) {
                resolve()
              } else {
                reject(new Error('service ' + this._name + ' access not granted for role ' + user.role))
              }
            } else {
              resolve() // default behaviour unsecured since double check with data access
            }
          }).catch(err => {
            console.error(err, 'failed to fetch roles')
            reject(new Error('roles not defined'))
          })
        } else {
          this.$app.$syncdb.roles.get({}).then(roles => {
            if (roles.anonymous) {
              if (_.find(roles.anonymous.exec, e => { return e === method })) {
                resolve()
              } else {
                reject(new Error('Execution not allowed for anonymous'))
              }
            } else {
              resolve() // default behaviour unsecured since double check with data access
            }
          }).catch(err => {
            console.error(err, 'failed to fetch roles')
            reject(new Error('roles not defined'))
          })
        }
      }).catch(err => {
        console.warn('failed to fetch user ' + userId + ' with err: ' + err)
        this.$app.$syncdb.roles.get({}).then(roles => {
          if (roles.anonymous && roles.anonymous.exec) {
            if (_.find(roles.anonymous.exec, e => { return e === method })) {
              resolve()
            } else {
              reject(new Error('Execution not allowed for anonymous'))
            }
          } else {
            resolve() // default behaviour unsecured since double check with data access
          }
        }).catch(err => {
          reject(err)
        })
      })
    })
  }

  /* registers public method */
  _addMethod(name, fct) {
    this._methods.push(name)
    this[name] = fct
  }

  /* detects public methods as per the naming rules */
  _detectMethods() {
    let properties = getAllMethods(this)

    for (let p of properties) {
      this._methods.push(p)
    }
  }

  /* register service logic for methods remote call through WS socket */
  _register() {
    console.log('[%s] methods registration [%s] with public options [%s]',
      this.name, this._methods, this._publicOptions)

    // on service method request
    this._io.on('service:' + this._name + ':request', async data => {
      try {
        let topic = 'service:' + this._name + ':' + data.method + ':' + data.token

        // prohibited methods:
        // _ -> internal/private
        // $ -> injected
        if (data.method[0] === '_' && data.method[0] === '$') {
          this._io.emit(topic, {
            err: 'private method call not allowed'
          })

          console.warn('private method [%s] call not allowed for service [%s]', data.method,
            this._name)

          return
        }

        // 2018/08/15: detokenize userID
        let decoded = {}
        try {
          decoded = await this.$app.$syncdb.users.checkToken({ token: data.userId })
          decoded = decoded || {}
        } catch (err) {
          console.warn('data service method ' + data.method + ' token check failed: ' + err)
        }

        // TBD: finalize new access concepts
        try {
          await this._checkAccess(data.method, decoded._id)
        } catch (err) {
          console.error(err, 'UNAUTHORIZED service [%s] method [%s] call: needs autorized user',
            this._name, data.method)

          this._io.emit(topic, {
            err: 'service ' + this._name + ' needs authorized user'
          })

          return
        }

        console.log('[%s]-> request [%s] - instance: [%s], user: [%s]',
          this._name, data.method, this.uuid, decoded._id)

        if (this[data.method]) {
          // injects userid for authorization check as per user's roles
          this[data.method](data.args, decoded._id).then(result => {
            console.log('[%s]-> response [%s] - user [%s]',
              this._name, topic, decoded._id)

            this._io.emit(topic, {
              result: result
            })
          }).catch(err => {
            this._io.emit(topic, { err: err + '' })
          })
        } else {
          this._io.emit(topic, {
            err: 'Method [' + data.method + '] is not available for service [%s]' + this._name
          })
        }
      } catch (err) {
        console.error(err, 'failed to call method [%s] for service [%s]', data.method, this._name)
      } // try/catch
    })

    // tells client that service is up
    this._io.emit('service:up', {
      name: this._name,
      methods: this._methods,
      options: this._publicOptions
    })
  }

  /* unregister service and inform client that service is down */
  _unregister() {
    console.log('[%s] unregistration', this.name)

    try {
      this._io.removeAllListeners('service:' + this._name + ':request')

      // tells client that service is down
      this._io.emit('service:down', {
        name: this._name
      })
    } catch (err) {
      console.error(err, 'failed to unregister', this._name)
    }
  }
}
