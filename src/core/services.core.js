import Service from './service.core'

export default class Services extends Service {
  constructor(io, options) {
    super(io, {
      name: 'services',
      ...options
    })

    // services references
    this._services = {}

    this._register()
  }

  /* register and instantiates a new service class: not exported */
  _registerService(ServiceClass, opts) {
    let service = new ServiceClass(this._io, opts)

    this._services[service.name] = service

    console.log('service [' + service.name + '] registered')
  }

  /* unregister and "uninstantiates" a service class: not exported */
  _unregisterService(service) {
    if (typeof service === 'object') {
      if (this._services[service.name]._destroy) {
        this._services[service.name]._destroy()
      }

      delete this._services[service.name]
    } else if (typeof service === 'string') {
      if (this._services[service]._destroy) {
        this._services[service]._destroy()
      }

      delete this._services[service]
    }
  }

  /* unregister all services: not exported */
  _unregisterAll() {
    for (let s in this._services) {
      this._unregisterService(s)
    }
  }
}
