import AccessControl from 'accesscontrol'
import Datum from '../core/datum.core'

/*
{
    admin: {
        video: {
            'create:any': ['*', '!views'],
            'read:any': ['*'],
            'update:any': ['*', '!views'],
            'delete:any': ['*']
        }
    },
    user: {
        video: {
            'create:own': ['*', '!rating', '!views'],
            'read:own': ['*'],
            'update:own': ['*', '!rating', '!views'],
            'delete:own': ['*']
        }
    }
}
*/
export default class Role extends Datum {
  constructor(syncdb) {
    super(syncdb, 'roles')

    this._ac = new AccessControl({})
    this.load().then(() => {
      this._loadedAndReady = true
    }).catch(err => {
      console.log(err, 'unable to load roles')
    })
  }

  load() {
    return new Promise((resolve, reject) => {
      this._findOne({}).then(doc => {
        if (doc) {
          try {
            delete doc._id
            delete doc._rev // couch
            delete doc.iio_collection // couch
            this._ac.setGrants(doc)
            resolve(doc)
          } catch (err) {
            console.log(err)
            reject(new Error('grants error'))
          }
        } else {
          reject(new Error('no roles defined'))
        }
      }).catch(err => reject(err))
    })
  }

  save(args, userId) {
    return new Promise((resolve, reject) => {
      if (!args) {
        reject(new Error('attempt to fill empty roles'))
        return
      }

      this.$syncdb.users._get(userId).then(user => {
        if (user) {
          this.permission({
            role: user.role,
            ressource: 'roles',
            action: 'updateAny'
          }).then(permission => {
            if (permission.granted) {
              this._findOne({}).then(doc => {
                let grants = this._ac.getGrants()
                this._updateOne({
                  _id: doc._id
                }, {
                  $set: grants
                }).then(() => {
                  resolve()
                }).catch(err => reject(err))
              }).catch(err => reject(err))
            } else {
              reject(new Error('access not granted'))
            }
          }).catch(err => reject(err))
        } else {
          reject(new Error('access not granted'))
        }
      }).catch(err => reject(err))
    })
  }

  permissionById(args, userId) {
    return new Promise((resolve, reject) => {
      if (!args) {
        reject(new Error('attempt to fill empty roles'))
        return
      }

      this.$syncdb.users.get(userId).then(user => {
        if (user) {
          args.role = user.role
          this.permission(args).then(permission => {
            resolve(permission)
          }).catch(err => reject(err))
        } else {
          reject(new Error('access not granted'))
        }
      }).catch(err => reject(err))
    })
  }

  permission(args) {
    return new Promise((resolve, reject) => {
      if (!args) {
        reject(new Error('attempt to get info for empty role'))
        return
      }

      try {
        let p = this._ac.can(args.role)[args.action](args.ressource)
        resolve(p)
      } catch (err) { 
        reject(err)
      }
    })
  }
}
