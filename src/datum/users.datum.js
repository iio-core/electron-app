import _ from 'lodash'
import jwt from 'jsonwebtoken'
import bcryptjs from 'bcryptjs'
import Datum from '../core/datum.core'
import ObjectID from 'bson-objectid'

/*
 * {
 *   email: 'vdrac@free.fr',
 *   username: 'ignitial',
 *   password: hash
 * }
 */
export default class User extends Datum {
  constructor(syncdb) {
    super(syncdb, 'users')
  }

  putNewUser(args, userId) {
    return new Promise(async (resolve, reject) => {
      this.$syncdb.roles.permissionById({
        ressource: 'roles',
        action: 'createAny'
      }, userId).then(permission => {
        if (permission.granted) {
          try {
            let user = args.user || args

            let password = Math.random().toString(36).slice(2, 12)
            let salt = bcryptjs.genSaltSync(10)
            let hash = bcryptjs.hashSync(password, salt)

            user.password = hash

            this.put(user, userId).then(result => {
              this.get({ _id: result._id }).then(doc => {
                resolve({ doc: doc })
              }).catch(err => reject(err))
            }).catch(err => reject(err))
          } catch (err) {
            reject(err)
          }
        } else {
          reject('access not granted')
        }
      }).catch(err => reject(err))
    })
  }

  checkPassword(args, userId) {
    return new Promise((resolve, reject) => {
      this._findOne({ 'username': args.username }).then(user => {
        if (user) {
          console.log(user, args.password)
          if (bcryptjs.compareSync(args.password, user.password)) {
            args.password = user.password
            args._id = user._id
            delete user.password

            let response = {
              user: user,
              token: jwt.sign(args, this.$syncdb.$config.auth.secret, {
                expiresIn: this.$syncdb.$config.auth.tokenTimeout || '5h'
              })
            }

            resolve(response)
          } else {
            reject(new Error('wrong password'))
          }
        } else {
          reject(new Error('user not found'))
        }
      }).catch(err => {
        console.log(err)
        reject(new Error('sign in failed'))
      })
    })
  }

  checkToken(args, userId) {
    return new Promise((resolve, reject) => {
      if (args.token) {
        try {
          jwt.verify(args.token, this.$syncdb.$config.auth.secret, (err, decoded) => {
            if (err) {
              reject(err)
            } else {
              resolve(decoded)
            }
          })
        } catch (err) { 
          reject(err)
          console.log(err)
        }
      } else {
        reject(new Error('token missing'))
      }
    })
  }
}
