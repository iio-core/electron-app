// Vue plugins
import configPlugin from './services/config.js'
import wsPlugin from './services/ws.js'
import authPlugin from './services/auth.js'
import servicesPlugin from './services/services.js'
import modulesPlugin from './services/modules.js'
import i18nPlugin from './services/i18n.js'
import utilsPlugin from './services/utils.js'
import syncdbPlugin from './services/syncdb.js'
import geoPlugin from './services/geo.js'

import {getRouter} from './router'
import {getStore} from './store'

import Datum from './core/datum.core'
import Module from './core/module.core'
import Service from './core/service.core'
import Services from './core/services.core'

// recursive components
import JSONFormItem from './components/iio/JSONFormItem.vue'
import JSONViewer from './components/iio/JSONViewer.vue'
// other components
import JSONForm from './components/iio/JSONForm.vue'
import FileDrop from './components/iio/FileDrop.vue'
import ColorPicker from './components/iio/ColorPicker.vue'
import Geo from './components/iio/Geo.vue'

export {
  configPlugin,
  wsPlugin,
  authPlugin,
  servicesPlugin,
  modulesPlugin,
  i18nPlugin,
  utilsPlugin,
  syncdbPlugin,
  geoPlugin,
  getRouter,
  getStore,
  JSONFormItem,
  JSONViewer,
  JSONForm,
  FileDrop,
  ColorPicker,
  Geo,
  Datum,
  Module,
  Service,
  Services
}
