import Router from 'vue-router'
import Login from '../views/Login.vue'
import Profile from '../views/Profile.vue'
import Users from '../views/Users.vue'
import EditUser from '../views/EditUser.vue'
import Admin from '../views/Admin.vue'

import Services from '../views/Services.vue'

var routerInstance

export function getRouter(Vue) {
  if (routerInstance) return routerInstance
  Vue.use(Router)

  routerInstance =  new Router({
    mode: 'hash',
    routes: [
      {
        path: '/login',
        component: Login
      },
      {
        path: '/profile',
        component: Profile,
        beforeEnter: (to, from, next) => {
          let token = localStorage.getItem('token')
          if (token && token !== 'null') {
            next()
          } else {
            next({ path: '/login' })
          }
        }
      },
      {
        path: '/users',
        component: Users,
        beforeEnter: (to, from, next) => {
          let token = localStorage.getItem('token')
          if (token && token !== 'null') {
            next()
          } else {
            next({ path: '/login' })
          }
        }
      },
      {
        path: '/edituser',
        component: EditUser,
        beforeEnter: (to, from, next) => {
          let token = localStorage.getItem('token')
          if (token && token !== 'null') {
            next()
          } else {
            next({ path: '/login' })
          }
        }
      },
      {
        path: '/admin',
        component: Admin,
        beforeEnter: (to, from, next) => {
          let token = localStorage.getItem('token')
          if (token && token !== 'null') {
            next()
          } else {
            next({ path: '/login' })
          }
        }
      },
      {
        path: '/services',
        component: Services
      },
      {
        path: '*',
        redirect: '/'
      }
    ]
  })

  return routerInstance
}
