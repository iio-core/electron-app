import bcrypt from 'bcryptjs'
import {getStore} from '../store'

var authPlugin = {
  install:
    function (Vue) {
      class AuthService extends Vue {
        constructor() {
          super()
          this.$store = getStore(Vue)

          this.uuid = Math.random().toString(36).slice(2)
        }

        authenticate(args) {
          return new Promise((resolve, reject) => {
            this.$syncdb.users.checkToken(args).then(decoded => {
              this._logged = decoded.username
              resolve()
            }).catch(err => {
              console.error(err, 'authentication failed')
              reject(err)
            })
          })
        }

        signin(args) {
          return new Promise((resolve, reject) => {
            this.$syncdb.users.checkPassword(args).then(response => {
              this._logged = args.username
              resolve(response)
            }).catch(err => {
              console.error(err, 'signin failed')
              reject(err)
            })
          })
        }

        signup(args) {
          let salt = bcrypt.genSaltSync(10)
          let hash = bcrypt.hashSync(args.password, salt)

          // update on the fly for clean persistency
          args.password = hash

          // delete for clean persistency
          if (args._auth) delete args['_auth']

          return new Promise((resolve, reject) => {
            this.$syncdb.users.put(args, this.$store.state.user._id).then(user => {
              if (user) {
                resolve(user)
              } else {
                console.error({ user: args }, 'impossible to create user')
                reject(new Error('impossible to create user'))
              }
            }).catch(err => {
              console.error(err, 'impossible to create user')
              reject(new Error('impossible to create user'))
            })
          })
        }

        signout() {
          return new Promise((resolve, reject) => {
            if (this._logged) {
              this._logged = undefined
              localStorage.token = undefined
              this.$store.commit('user', null)
              resolve()
            } else {
              console.error('not logged when asking for logout')
              reject(new Error('not logged'))
            }
          })
        }

        chpwd(args) {
          return new Promise((resolve, reject) => {
            if (this._logged === args.username) {
              this.$syncdb.users.get({ 'username': args.username }, this.$store.state.user._id).then(user => {
                if (user) {
                  // compute password hash
                  let salt = bcrypt.genSaltSync(10)
                  let hash = bcrypt.hashSync(args.newPassword, salt)
                  user.password = hash

                  this.$app.$syncdb.users.put(user).then(() => {
                    resolve()
                  }).catch(err => {
                    console.error(err, 'impossible to save user [%s]', args.username)
                    reject(new Error('impossible to save user'))
                  })
                } else {
                  console.error('impossible to find user [%s]', args.username)
                  reject(new Error('impossible to find user'))
                }
              }).catch(err => {
                console.error(err, 'impossible to find user [%s]', args.username)
                reject(new Error('impossible to find user'))
              })
            } else {
              console.error({ user: args }, 'must be logged')
              reject(new Error('must be logged'))
            }
          })
        }
      }

      Vue.prototype.$auth = new AuthService()
    }
}

export default authPlugin
