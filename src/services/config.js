import fs from 'fs'
import path from 'path'
import parse from 'csv-parse/lib/sync'

import electron from 'electron'
const app = electron.remote.app

var configPlugin = {
  install:
    function (Vue) {
      class ConfigService extends Vue {
        constructor() {
          super()
        }

        initialize() {
          console.log('NODE_ENV=' + process.env.NODE_ENV, process.cwd())
          let configPath = path.join(app.getAppPath(), 'config', 'config.json')
          if (process.env.NODE_ENV === 'development') {
            configPath = path.join('public', 'config', 'config.json')
          }

          let rawJsonCfg = fs.readFileSync(configPath, 'utf8')
          let config = JSON.parse(rawJsonCfg)

          for (let cfg in config) {
            this[cfg] = config[cfg]
          }

          let csvPath = path.join(app.getAppPath(), 'config', 'i18n', 'i18n_translations.csv')

          if (process.env.NODE_ENV === 'development') {
            csvPath = path.join('public', 'config', 'i18n', 'i18n_translations.csv')
          }

          let translations = fs.readFileSync(csvPath, 'utf8')
          translations = parse(translations, { delimiter: ';' })
          this.i18n.languages = translations[0].slice(1)
          for (let i = 1; i < translations.length; i++) {
            this.i18n.data[translations[i][0]] = translations[i].slice(1)
          }
        }
      }

      Vue.prototype.$config = new ConfigService()
      Vue.prototype.$config.initialize()
    }
}

export default configPlugin
