import axios from 'axios'

var geoPlugin = {
  install:
    function (Vue) {
      class GeoService extends Vue {
        constructor() {
          super()
          this.uuid = Math.random().toString(36).slice(2)
        }
      
        reverse(args) {
          return new Promise((resolve, reject) => {
            let baseURL = 'http://nominatim.openstreetmap.org/reverse'
            let params = 'format=jsonv2&lat=' + args.lat + '&lon=' + args.lon
            let url = baseURL + '?' + params
      
            axios.post(url, {}).then(response => {
              if (response.code !== 200) {
                reject(response)
              } else {
                let result = JSON.parse(response.body)
      
                resolve({
                  location: {
                    lat: args.lat,
                    lon: args.lon
                  },
                  address: result.address,
                  name: result.name
                })
              }
            })
          })
        }
      
        location(args) {
          let address = args.address
      
          return new Promise((resolve, reject) => {
            let baseURL = 'http://nominatim.openstreetmap.org/search/'
            let params = 'format=jsonv2'
            let url = baseURL + address + '?' + params
      
            axios.post(url, {}).then(function(response) {
              if (response.code !== 200) {
                reject(response)
              } else {
                var result = JSON.parse(response.body)[0]
                if (result) {
                  resolve({
                    lat: result.lat,
                    lon: result.lon
                  })
                } else {
                  reject(new Error('not found'))
                }
              }
            })
          })
        }
      }
      
      Vue.prototype.$geo = new GeoService()
    }
}

export default geoPlugin
