import _ from 'lodash'
import {getStore} from '../store'

var i18nPlugin = {
  install:
    function (Vue) {
      class I18n extends Vue {
        constructor() {
          super()
          this.$store = getStore(Vue)
          this._language = 'fr-FR' // navigator.language
          this._languages = []
          this._translations = {}
          this._translationsInit = {}
        }
      
        async initialize() {
          if (this.$config) {
            this.addTranslations(this.$config.i18n.data)
            this._translationsInit = _.cloneDeep(this._translations)
            this._languages = this.$config.i18n.languages
            this.setTranslation(this._language)
          } else {
            console.error('error initializing i18n')
          }
        }
      
        addTranslations(data) {
          // console.log('before', this._translations, data)
          _.merge(this._translations, data)
          // console.log('after', this._translations)
          this.setTranslation(this._language)
        }
      
        resetTranslation() {
          this._translations = this._translationsInit
          this.setTranslation(this._language)
        }
      
        setTranslation(lang) {
          let index = this._languages.indexOf(lang)
          let st = {}
          for (let t in this._translations) {
            if (index < 0) {
              st[t] = t
            } else {
              st[t] = this._translations[t][index]
            }
          }
          this.$store.commit('translation', st)
          this._language = lang
      
          this.$emit('translation')
          // console.log('lang set to ', lang)
        }
      }

      Vue.prototype.$i18n = new I18n()
      Vue.prototype.$i18n.initialize()
      
      Vue.prototype.$t = function(what, ...params) {
        // BUG: some kind of rest params bug when eval
        params = params // eslint-disable-line no-self-assign

        if (this.$store.state.translation[what]) {
          // protect against malicious code (even if internal)
          //   what.length < 256
          // To Be Improved
          if (what.match(/`/) && what.length < 256) {
            return eval(this.$store.state.translation[what]) // eslint-disable-line no-eval
          } else {
            return this.$store.state.translation[what]
          }
        } else {
          if (what && what.match(/`/) && what.length < 256) {
            return eval(what) // eslint-disable-line no-eval
          }
          return what
        }
      }
    }
}

export default i18nPlugin
