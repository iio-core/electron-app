import {getStore} from '../store'
import * as d3 from 'd3'

var servicesPlugin = {
  install:
    function (Vue) {
      class ServicesService extends Vue {
        constructor() {
          super()
          this.$store = getStore(Vue)
          this.uuid = Math.random().toString(36).slice(2)

          // services instances (by name)
          this.services = {}

          // module based services (== unified)
          this.modules = {}

          // services info data (by name)
          this.servicesDico = {}
        }

        initialize() {

          try {
            // watch connection status
            this.$ws.socket.on('disconnect', () => {
              this.$store.commit('connected', false)
              this.$services.$emit('app:disconnected')

              this.$ws.socket.once('connect', () => {
                if (!this.$store.state.connected) this.$emit('app:connected')
                this.$store.commit('connected', true)
              })

              this.$ws.socket.once('reconnect', () => {
                if (!this.$store.state.connected) this.$emit('app:connected')
                this.$store.commit('connected', true)
              })
            })

            // event service:up means a new modules service available
            this.$ws.socket.on('service:up', service => {
              if (service) {
                // register each method for further call
                for (let i = 0; i < service.methods.length; i++) {
                  this.register(service.name, service.methods[i])
                }
        
                this.servicesDico[service.name] = service
        
                if (service.options && service.options.uiComponentInjection) {
                  let all = []
                  let baseUrl = '/api/services/' + service.name + '/'
                  let imagesBaseUrl = '/api/images/' + service.name + '/'
                  let urlOptions = '?api_key=' + this.$config.rest.apiKeys[0]
        
                  let jsURL = baseUrl + 'build.js' + urlOptions
                  let cssURL = baseUrl + 'build.css' + urlOptions
        
                  this.servicesDico[service.name].baseUrl = baseUrl
                  this.servicesDico[service.name].imagesBaseUrl = baseUrl
                  this.servicesDico[service.name].urlOptions = urlOptions
                  this.servicesDico[service.name].options.description.icon =
                    imagesBaseUrl +
                    this.servicesDico[service.name].options.description.icon + urlOptions
        
                  // translations
                  this.$i18n.addTranslations(
                    this.servicesDico[service.name].options.description.i18n)
        
                  // effective load
                  all.push(this.$utils.loadAsync(this.$ws.url + cssURL, 'css'))
                  all.push(this.$utils.loadAsync(this.$ws.url + jsURL, 'js'))
        
                  Promise.all(all).then(added => {
                    // eval(`require('${service.name}')`)
                    this.servicesDico[service.name].ready = true
                    this.servicesDico[service.name].domElements = []
        
                    for (let a of added) {
                      this.servicesDico[service.name].domElements.push(a)
                    }
        
                    // tells locally (client side) that a service is up
                    this.$emit('service:up', service)
                  }).catch(err => console.log(err))
                } else {
                  this.servicesDico[service.name].ready = true
                  // tells locally (client side) that a service is up
                  this.$emit('service:up', service)
                }
              } else {
                console.log('got undefined service')
              }
            })
        
            // a service has been shut down
            this.$ws.socket.on('service:down', service => {
              // send event before destroying data
              this.$emit('service:down', service.name, this.servicesDico[service.name])
        
              // if dom elements, then remove
              if (this.servicesDico[service.name].domElements) {
                for (let domElId of this.servicesDico[service.name].domElements) {
                  d3.select('#' + domElId).remove()
                }
              }
        
              // deletes registered reference
              delete this[service.name]
              delete this.servicesDico[service.name]
            })
          } catch (err) {
            console.log(err)
          }
        }

        register(service, method) {
          this[service] = this[service] || {}

          this[service][method] = args => {
            return new Promise((resolve, reject) => {
              let token = Math.random().toString(36).slice(2)
              let topic = 'service:' + service + ':' + method + ':' + token

              let timeout = setTimeout(() => {
                this.$ws.socket.off(topic)
                reject(new Error('timeout for ' + topic))
              }, this.$config.gateway.rpcTimeout)

              this.$ws.socket.once(topic, data => {
                clearTimeout(timeout)
                if (data.err) {
                  reject(data.err)
                } else {
                  resolve(data.result)
                }
              })

              let fullArgs = {
                args: args,
                token: token,
                method: method,
                userId: localStorage.token
              }

              this.$ws.socket.emit('service:' + service + ':request', fullArgs)
            })
          }
        }

        waitForService(name, delay) {
          return new Promise((resolve, reject) => {
            var checkTimeout

            var checkInterval = setInterval(() => {
              if (this[name]) {
                clearInterval(checkInterval)
                clearTimeout(checkTimeout) // nothing if undefined

                resolve(this[name])
              }
            }, 100)

            if (delay !== 0) {
              checkTimeout = setTimeout(() => {
                if (checkInterval) {
                  clearInterval(checkInterval)
                  reject(new Error('Timeout: service ' + name + ' is not available'))
                }
              }, delay || 5000)
            }
          })
        }

        waitForServiceProperty(name, property, delay) {
          return new Promise((resolve, reject) => {
            var checkTimeout

            var checkInterval = setInterval(() => {
              if (this[name] && this[name][property]) {
                clearInterval(checkInterval)
                clearTimeout(checkTimeout) // nothing if undefined

                resolve(this[name][property])
              }
            }, 100)

            checkTimeout = setTimeout(() => {
              if (checkInterval) {
                clearInterval(checkInterval)
                reject(new Error('Timeout: service ' + name + '\'s property ' + property + ' is not available'))
              }
            }, delay || 5000)
          })
        }
      }

      Vue.prototype.$services = new ServicesService()
      Vue.prototype.$services.initialize()
    }
}

export default servicesPlugin
