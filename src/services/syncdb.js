import levelup from 'levelup'
import leveldown from 'leveldown'
import _ from 'lodash'
import {join} from 'path'

import {getStore} from '../store'

import Notifications from '../datum/notifications.datum'
import Roles from '../datum/roles.datum'
import Users from '../datum/users.datum'

import electron from 'electron'
const app = electron.remote.app

var SyncDBPlugin = {
  install:
    function (Vue) {
      class SyncDBService extends Vue {
        constructor() {
          super()
          this.$store = getStore(Vue)
          this.uuid = Math.random().toString(36).slice(2)
        }

        async initialize(populate, additionalCollections) {
          try {
            if (this.$config.offline && this.$config.offline.dbName && this.$config.offline.dbName !== '') {
              let uri = join(process.cwd(), this.$config.offline.folder,
                this.$config.offline.dbName + '.db')

              if (process.env.NODE_ENV === 'development') {
                uri = join(this.$config.offline.folder, this.$config.offline.dbName + '.db')
              }

              this._db = levelup(leveldown(uri))
            } else {
              throw new Error('db this.$config not defined')
            }

            this.collections = (this.$config.offline ? this.$config.offline.collections : []) || []

            this.createCollection('roles', Roles)
            this.createCollection('users', Users)
            this.createCollection('notifications', Notifications)

            if (additionalCollections) {
              for (let c of additionalCollections) {
                this.createCollection(c.name, c.klass)
              }
            }

            if (this.$config.offline && this.$config.offline.activated) {
              this.$services.$on('app:login', this.onLogin.bind(this))

              if (!this.$store.state.user) {
                console.log('SYNC REQUEST FOR USERS ONLY - NOT CONNECTED')
                await this.syncFromRemote('users')
              }
            } else {
              console.log('offline sync not activated')
            }

            try {
              await this.roles._findOne({})
            } catch (err) {
              console.log('empty db')
            }

            if (this.$config.db.forcePopulate) {
              try {
                console.log(JSON.stringify(this.$config.db, null, 2))
                await populate(this._db)
              } catch (err) {
                console.log(err)
              }
            }

            this.ready = true
          } catch(err) {
            console.log('syncdb init failed', err)
          }
        }

        createCollection(name, Klass) {
          Klass.prototype.$app = this
          Klass.prototype.$datum = this

          this[name] = new Klass(this)
        }

        async syncFromRemote(collection) {
          console.log('SYNC FROM REMOTE REQUESTED', collection)
          try {
            if (collection) {
              this[collection].mustSync = true
              await this[collection]._syncFromRemote()
            } else {
              // only synched collections: this.collections < this.
              for (let collection of this.collections) {
                console.log('synching local from remote', collection)
                if (this[collection]) {
                  this[collection].mustSync = true
                  await this[collection]._syncFromRemote()
                } else {
                  console.log('data discrepency for ' + collection + ' -> not synched')
                }
              }
            }
          } catch (err) {
            console.log('SYNC failed', err)
          }
        }

        async syncToRemote(collection) {
          console.log('SYNC FROM REMOTE REQUESTED', collection)
          try {
            if (collection) {
              await this[collection]._syncToRemote()
            } else {
              // only synched collections: this.collections < this.
              for (let collection of this.collections) {
                console.log('synching remote from local', collection)
                if (this[collection]) {
                  await this[collection]._syncToRemote()
                } else {
                  console.log('data discrepency for ' + collection + ' -> not synched')
                }
              }
            }
          } catch (err) {
            console.log('SYNC failed', err)
          }
        }

        onLogin() {
          this.syncFromRemote()
        }
      }

      Vue.prototype.$syncdb = new SyncDBService()
    }
}

export default SyncDBPlugin
