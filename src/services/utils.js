import fs from 'fs'
import path from 'path'

import moment from 'moment'
import 'moment/locale/fr'
import 'moment/locale/es'

import * as d3 from 'd3'
import _ from 'lodash'

import empty from 'json-schema-empty'

import electron from 'electron'
const app = electron.remote.app

var utilsPlugin = {
  install:
    function (Vue) {
      class UtilsService extends Vue {
        constructor() {
          super()
          this.uuid = Math.random().toString(36).slice(2)
        }

        initialize() {
          this.url = (this.$config.gateway.secured ? 'https://' : 'http://') +
            this.$config.gateway.host + ':' + this.$config.gateway.port
        }

        /* used for deep update in JS object given a path description */
        updatePropertyByPath(obj, path, val, removal) {
          // console.log(JSON.stringify(obj, null, 2), path, val)
          if (path.match(/\./)) {
            // console.log('path=', path, ', val=', JSON.stringify(val))
            let pathArray = path.split('.')
            let propPath = pathArray.slice(1).join('.')
            let prop = pathArray[0]
            if (prop.match(/\[.*\]/)) {
              let arrName = prop.slice(0, prop.indexOf('['))
              let idx = parseInt(path.match(/\[.*\]/)[0].replace('[', '').replace(']', ''))

              obj[arrName] = obj[arrName] || []
              obj[arrName][idx] = obj[arrName][idx] || {}
              this.updatePropertyByPath(obj[arrName][idx], propPath, val, removal)
            } else {
              this.updatePropertyByPath(obj[prop], propPath, val, removal)
            }
          } else if (path.match(/\[/)) {
            let count = path.match(/\[/g).length
            if (count === 1) {
              let idx = parseInt(path.match(/\[.*\]/)[0].replace('[', '').replace(']', ''))
              let prop = path.match(/.*\[/)[0].replace('[', '')

              if (!removal) {
                obj[prop][idx] = val
                // console.log('idx', idx, 'prop', prop, obj[prop][idx])
              } else {
                obj[prop].splice(idx, 1)
                // console.log('idx removal ', idx, 'prop', prop)
              }
            } else {
              let matches = path.match(/\[\d\]/g)
              let idxs = _.map(matches, function(e) {
                return parseInt(e.replace('[', '').replace(']', ''))
              })

              let prop = path.match(/\w+\[/)[0].replace('[', '')

              if (!removal) {
                // manage array of arrays: to be improved
                // console.log('prop = ', obj[prop], 'val = ', val, JSON.stringify(obj[prop]))

                obj[prop] = obj[prop] || []
                // console.log(JSON.stringify(obj[prop]))
                let fullObj = obj[prop]
                for (let i = 0; i < idxs.length; i++) {
                  if (i < idxs.length - 1) {
                    fullObj[idxs[i]] = fullObj[idxs[i]] || []
                    fullObj = fullObj[idxs[i]]
                  } else {
                    fullObj[idxs[i]] = parseFloat(val)
                  }
                }

                // console.log('idxs', idxs, 'prop', prop, fullObj, JSON.stringify(obj[prop]))
              } else {
                obj[prop].splice(idxs[0], 1)
                // console.log('idx removal ', idx, 'prop', prop)
              }
            }
          } else {
            obj[path] = val
          }
        }

        fromNow(date) {
          let lang = this.$i18n._language.split('-')[0]
          return date ? moment(date).locale(lang).fromNow() : ''
        }

        prettyDate(date) {
          let lang = this.$i18n._language.split('-')[0]
          return date ? moment(date).locale(lang).format('DD MMMM YYYY') : ''
        }

        prettyDateAndTime(date) {
          let lang = this.$i18n._language.split('-')[0]
          return date ? moment(date).locale(lang).format('DD MMMM YYYY  hh:mm') : ''
        }

        loadAsync(what, whichKind) {
          return new Promise((resolve, reject) => {
            var req = new XMLHttpRequest()

            req.addEventListener('error', function(err) {
              reject(err)
            })

            // report progress events
            req.addEventListener('progress', function(event) {

            }, false)

            // load responseText into a new script element
            req.addEventListener('load', function(event) {
              let e = event.target
              let uid = 'la_' + Math.random().toString(36).slice(2, 12)

              if (e.responseText) {
                var containerElement

                switch (whichKind) {
                  case 'js':
                    containerElement = document.createElement('script')
                    break
                  case 'css':
                    containerElement = document.createElement('style')
                    break
                  default:
                    containerElement = document.createElement('script')
                }

                containerElement.innerHTML = e.responseText
                containerElement.id = uid
                console.log(what, whichKind, e.responseText)
                // or: s[s.innerText!=undefined?"innerText":"textContent"] = e.responseText
                document.documentElement.appendChild(containerElement)
              }

              resolve(uid)

              /*
              s.addEventListener('load', function() {

              })
              */
            }, false)

            req.open('GET', what)
            req.send()
            // console.log('get', what)
          })
        }

        token() {
          return localStorage.token
        }

        waitForDOMReady(selector, delay = 5000) {
          return new Promise((resolve, reject) => {
            let checkInterval = setInterval(() => {
              let el = d3.select(selector)
              if (!el.empty()) {
                clearInterval(checkInterval)
                clearTimeout(timeout)
                resolve(el)
              }
            }, 50)

            let timeout = setTimeout(() => {
              clearInterval(checkInterval)
              reject(new Error('timeout for DOM element ' + selector))
            }, delay)
          })
        }

        waitForProperty(obj, prop, delay) {
          return new Promise((resolve, reject) => {
            var checkTimeout

            var checkInterval = setInterval(() => {
              if (obj[prop]) {
                clearInterval(checkInterval)
                clearTimeout(checkTimeout) // nothing if undefined

                resolve(obj[prop])
              }
            }, 100)

            checkTimeout = setTimeout(() => {
              if (checkInterval) {
                clearInterval(checkInterval)
                reject(new Error('Timeout: property ' + prop + ' is not available'))
              }
            }, delay || 5000)
          })
        }

        /* url for images */
        imgUrl(relativePath) {
          return 'file://' + path.join(process.cwd(), relativePath)
        }

        /* JSON Schema generation */
        generateDataFormJSONSchema(args) {
          return empty(args.schema)
        }

        /* provide app info */
        info() {
          return new Promise((resolve, reject) => {
            if (!this.appInfo) {
              let url = path.join(app.getAppPath(), 'package.json')
              if (process.env.NODE_ENV === 'development') {
                url = 'package.json'
              }

              fs.readFile(url, 'utf8', (err, result) => {
                if (err) {
                  reject(err)
                } else {
                  this.appInfo = {
                    name: JSON.parse(result).name,
                    version: JSON.parse(result).version
                  }
                  resolve(this.appInfo)
                }
              })
            } else {
              resolve(this.appInfo)
            }
          })
        }
      }

      Vue.prototype.$utils = new UtilsService()
      Vue.prototype.$utils.initialize()
    }
}

export default utilsPlugin
