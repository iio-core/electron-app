import {getStore} from '../store'
import io from 'socket.io-client'

var wsPlugin = {
  install:
    function (Vue) {
      const storeInstance = getStore(Vue)

      class WSService extends Vue {
        constructor() {
          super()
          this.$store = storeInstance
          this.uuid = Math.random().toString(36).slice(2)
        }

        initialize() {
          this.url = (this.$config.gateway.secured ? 'https://' : 'http://') +
            this.$config.gateway.host + ':' + this.$config.gateway.port
          console.log('connecting to gateway at [' + this.url + ']')
          this.socket = io.connect(this.url,
            { secure: this.$config.gateway.secured, reconnect: true, rejectUnauthorized : false })

          this.socket.on('connect', () => {
            this.$store.commit('connected', true)
            this.$emit('connected')
            console.log('connected to gateway at [' + this.url + ']')
          })

          this.socket.on('connect_error', err => {
            console.log('ws connection failed', err)
          })

          this.socket.on('error', err => {
            console.log('ws error', err)
          })
        }

        resetLocalCredentials() {
          localStorage.token = null
          this.$store.commit('user', null)
          this.socket._logged = false
        }
      }

      Vue.prototype.$ws = new WSService()
      Vue.prototype.$ws.initialize()
    }
}

export default wsPlugin
