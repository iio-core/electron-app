import Vuex from 'vuex' 
import createPersistedState from 'vuex-persistedstate'

var storeInstance

export function getStore(Vue) {
  if (storeInstance) return storeInstance
  console.log('creating new store...')
  Vue.use(Vuex)

  const state = {
    translation: '',
    user: null,
    otherUser: null,
    geoCenter: null,
    toolbarColor: 'transparent',
    flatToolbar: true,
    darkTheme: false,
    section: null,
    showSection: true,
    connected: false
  }
  
  const mutations = {
    translation(state, value) {
      state.translation = value
    },
    user(state, value) {
      state.user = value
    },
    otherUser(state, value) {
      state.otherUser = value
    },
    geoCenter(state, value) {
      state.geoCenter = value
    },
    toolbarColor(state, value) {
      state.toolbarColor = value
    },
    flatToolbar(state, value) {
      state.flatToolbar = value
    },
    darkTheme(state, value) {
      state.darkTheme = value
    },
    section(state, value) {
      state.section = value
    },
    showSection(state, value) {
      state.showSection = value
    },
    connected(state, value) {
      state.connected = value
    }
  }
  
  const actions = {}
  
  storeInstance = new Vuex.Store({
    state,
    mutations,
    actions,
    plugins: [ createPersistedState() ]
  })

  return storeInstance
}
