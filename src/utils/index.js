const fs = require('fs')
const promisify = require('util').promisify

const _RESERVED_METHODS = [
  '__defineGetter__',
  '__defineSetter__',
  '__lookupGetter__',
  '__lookupSetter__',
  'hasOwnProperty',
  'isPrototypeOf',
  'propertyIsEnumerable',
  'toLocaleString',
  'toString',
  'valueOf',
  'name',
  'addListener',
  'emit',
  'eventNames',
  'getMaxListeners',
  'listenerCount',
  'listeners',
  'on',
  'off',
  'once',
  'prependListener',
  'prependOnceListener',
  'removeAllListeners',
  'removeListener',
  'setMaxListeners',
  'rawListeners'
]

function _promisifyAllFunctions (object) {
  for (const key of getAllMethods(object)) {
    const func = object[key]
    if (typeof func === 'function') {
      object[`${key}Async`] = promisify(func)
    }
  }
}

export function promisifyAll(object) {
  _promisifyAllFunctions(object)

  const proto = Object.getPrototypeOf(object)
  if (proto) {
    _promisifyAllFunctions(proto)
  }

  return object
}

export function fileExists(path) {
  try {
    fs.accessSync(path)
    return true
  } catch (err) {
    return false
  }
}

export let getAllMethods = obj => {
  let props = []

  do {
    const l = Object.getOwnPropertyNames(obj)
      .concat(Object.getOwnPropertySymbols(obj).map(s => s.toString()))
      .sort()
      .filter((p, i, arr) => {
        return !p.match(/arguments|caller/) &&    // ignore strict mode restrictions
          typeof obj[p] === 'function' &&         // only the methods
          p !== 'constructor' &&                  // not the constructor
          (i == 0 || p !== arr[i - 1]) &&         // not overriding in this prototype
          p[0] !== '_' &&                         // not internals
          p[0] !== '$' &&                         // not injected
          _RESERVED_METHODS.indexOf(p) === -1 &&  // not reserved
          props.indexOf(p) === -1                 // not overridden in a child
      })

    props = props.concat(l)
  } while (
    (obj = Object.getPrototypeOf(obj)) &&   //walk-up the prototype chain
    Object.getPrototypeOf(obj)              //not the the Object prototype methods (hasOwnProperty, etc...)
  )

  return props
}

export let uuid = () => {
  return Math.random().toString(36).slice(2)
}

/* wait for obj property to be defined */
export let waitForPropertyInit = (obj, name, delay) => {
  delay = delay || 5000

  return new Promise((resolve, reject) => {
    let checkTimeout

    let checkInterval = setInterval(() => {
      if (obj[name]) {
        clearInterval(checkInterval)
        clearTimeout(checkTimeout) // nothing if undefined

        resolve(obj[name])
      }
    }, 100)

    checkTimeout = setTimeout(() => {
      if (checkInterval) {
        clearInterval(checkInterval)
        reject(new Error('timeout: [' + name + '] property is not available'))
      }
    }, delay)
  })
}
